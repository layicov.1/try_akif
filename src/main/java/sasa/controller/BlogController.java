package sasa.controller;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import sasa.model.Blog;
import sasa.service.BlogService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class BlogController {
    private final BlogService blogService;
    @GetMapping
    public List<Blog> getAllBlog(){
        return blogService.getAllBlog();
    }

    @PostMapping
    public String createBlog(@RequestBody Blog blog){
        return blogService.createBlog(blog);
    }

    @DeleteMapping("/{id}")
    public String deleteBlog(@PathVariable Integer id){
        return blogService.deleteBlog(id);
    }
    @GetMapping("/{id}")
    public Blog getOneBlog(@PathVariable Integer id){
        return blogService.getOneBlog(id);
    }
}
