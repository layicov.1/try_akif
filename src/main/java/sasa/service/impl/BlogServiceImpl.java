package sasa.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import sasa.model.Blog;
import sasa.repo.BlogRepository;
import sasa.service.BlogService;

import java.util.List;
@Service
@RequiredArgsConstructor
public class BlogServiceImpl implements BlogService {
    private final BlogRepository blogRepository;
    @Override
    public List<Blog> getAllBlog() {
        return blogRepository.findAll();
    }

    @Override
    public String createBlog(Blog blog) {
        blogRepository.save(blog);
        return "Blog add";
    }

    @Override
    public String deleteBlog(Integer id) {
        blogRepository.deleteById(id);
        return "blog silindi";
    }

    @Override
    public Blog getOneBlog(Integer id) {
        return blogRepository.findById(id).get();
    }
}
