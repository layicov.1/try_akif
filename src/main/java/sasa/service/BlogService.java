package sasa.service;

import sasa.model.Blog;

import java.util.List;

public interface BlogService {
    List<Blog> getAllBlog();

    String createBlog(Blog blog);

    String deleteBlog(Integer id);

    Blog getOneBlog(Integer id);
}
