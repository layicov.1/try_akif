package sasa.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import sasa.model.Blog;

public interface BlogRepository extends JpaRepository<Blog,Integer> {
}
