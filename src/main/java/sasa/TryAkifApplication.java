package sasa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TryAkifApplication {

    public static void main(String[] args) {
        SpringApplication.run(TryAkifApplication.class, args);
    }

}
